latest=$(git log -1 --pretty=%B)
json="{
\"channel\": \"#ua-bootstrap\",
\"username\":\"hexo\",
\"icon_emoji\":\":beryl:\",
\"attachments\":[{
    \"fallback\": \"Branch \`${BRANCH}\` ready for review at: ${S3URL}/review/${BRANCH}\",
    \"color\":\"#FF6347\",
    \"author_name\": \"uadigital/ua-bootstrap\",
    \"author_link\": \"https://bitbucket.org/uadigital/ua-bootstrap\",
    \"title\": \"${S3URL}/review/${BRANCH}\",
    \"title_link\": \"${S3URL}/review/${BRANCH}\",
    \"text\": \"$latest\"
    }]
}"

curl -s -d "payload=$json" "https://hooks.slack.com/services/${SLACK_SECRET}"
